#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 15:14:11 2019
https://dev.to/terrillo/a-quick-introduction-to-python-multiprocessing-pools-speeding-up-your-workflow-17op
@author: Eduardo Tusa
https://pythonprogramming.net/values-from-multiprocessing-intermediate-python-tutorial/
"""

from amse1 import AMS_LiDAR
import sys


def AMS_experiment( plotid ):
    # m1 is m_lin for Ellipsoid, or is m_hr for Cylinder << coefficients[0]
    # m2 is m_log for Ellipsoid, or is m_dh for Cylinder << coefficients[1]
    AMS_plot = AMS_LiDAR( plot_id = plotid,
            max_number_iterations = 100,
            min_feature_distance = 0.000001,
            min_cluster_distance = 0.4,
            min_samples = 15,
            threshold_height = 1.5,
            factor = 2,
            m1 = 0.199,
            m2 = 0.702,
            m3 = 1.5,
            root_laz = "/nfs/home/eduardo.tusa-jumbo/plots/",
            root_csv = "/nfs/home/eduardo.tusa-jumbo/tests/",
            root_las = "/nfs/home/eduardo.tusa-jumbo/sublas/",
            out_filename = plotid + ".csv"
            )
    
    AMS_plot.Run_AMS_Ellipsoid()
    print( AMS_plot.time_execution )


AMS_experiment( sys.argv[1] )