#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 10:50:07 2019

@author: Eduardo Tusa
"""
import numpy as np
from numpy import linalg as LA
# pip install pandas
import pandas as pd
# pip install laspy
from laspy.file import File
from sklearn.neighbors import KDTree
from sklearn.cluster import DBSCAN
import time

class AMS_LiDAR:
    def __init__(self, plot_id, max_number_iterations, min_feature_distance, min_cluster_distance, min_samples, threshold_height, factor, m1, m2, m3, root_laz, root_csv, root_las, out_filename):
        self.plot_id = plot_id
        self.max_number_iterations = max_number_iterations
        self.min_feature_distance = min_feature_distance
        self.min_cluster_distance = min_cluster_distance
        self.min_samples =  min_samples
        self.threshold_height = threshold_height
        self.factor = factor
        self.m1 = m1
        self.m2 = m2
        self.m3 = m3
        self.root_laz = root_laz
        self.root_csv = root_csv
        self.root_las = root_las
        self.out_filename = out_filename
        
        
    def Load_Data(self):        
        inFile = File(self.root_laz + self.plot_id + ".laz", mode='r')
        all_xyz_points = np.vstack((inFile.x, inFile.y, inFile.z)).transpose()
        decimated_points = all_xyz_points[::self.factor]
        self.xyz_points =  decimated_points[ decimated_points[:,2] >= self.threshold_height ]
        self.tree = KDTree(self.xyz_points[:,0:2], leaf_size=2)
        
        outFile = File(self.root_las + self.plot_id + ".las", mode='w', header=inFile.header)
        new_points = inFile.points[::self.factor]
        outFile.points = new_points[ decimated_points[:,2] >= self.threshold_height ]
        outFile.close()
    
    def Run_AMS_Cylinder(self):
        self.Load_Data()
        self.Mean_Shift_Cylinder()
        self.Cluster_Points()
        self.Write_csv()
    
    
    def Run_AMS_Ellipsoid(self):
        self.Load_Data()
        start = time.time()
        self.Mean_Shift_Ellipsoid()
        self.Cluster_Points()
        end = time.time()
        self.time_execution = end - start
        self.Write_csv()


    def In_Cylinder_backup(self, meanfeature, rk, hk):
        x0 = meanfeature[0]
        y0 = meanfeature[1]
        x = self.xyz_points[:,0]
        y = self.xyz_points[:,1]
        
        xyz = self.xyz_points[ (x-x0)*(x-x0) + (y-y0)*(y-y0) <= rk + 0.01  ] 
        
        cylinder_slice = xyz[ xyz[:,2] >= meanfeature[2] - 0.50*hk ]
        slice_arr = cylinder_slice[ cylinder_slice[:,2] <= meanfeature[2] + 0.50*hk ]
        return slice_arr
    
    
    def In_Cylinder(self, meanfeature, rk, hk):
#        tree = KDTree(self.xyz_points[:,0:2], leaf_size=2)
        # Select the neighbors around a circular profile
        spatial_meanfeature = np.array( [ meanfeature[0:2].tolist() ] )
        ind = self.tree.query_radius( spatial_meanfeature, r = rk + 0.01 )
        xyz = self.xyz_points[ ind[0] ]              
        
        cylinder_slice = xyz[ xyz[:,2] >= meanfeature[2] - 0.50*hk ]
        slice_arr = cylinder_slice[ cylinder_slice[:,2] <= meanfeature[2] + 0.50*hk ]
        return slice_arr
    
    
    def In_Superellipsoid(self, meanfeature, rk, ak ):
#        tree = KDTree(self.xyz_points[:,0:2], leaf_size=2)
        # Select the neighbors around a circular profile
        spatial_meanfeature = np.array( [ meanfeature[0:2].tolist() ] )
        ind = self.tree.query_radius( spatial_meanfeature, r = rk + 0.01 )
        xyz = self.xyz_points[ ind[0] ]
        
        hmax = np.max( xyz[:,2] )          # Compute maximum height
        
        x0 = meanfeature[0]
        y0 = meanfeature[1]
        z0 = meanfeature[2]
        x = xyz[:,0]
        y = xyz[:,1]
        z = xyz[:,2]
        
        slice_arr = xyz[ (ak**self.m3) * ( (x-x0)**2 + (y-y0)**2 )**(0.5*self.m3) + (rk**self.m3) * (np.abs(z-z0))**self.m3 < rk**self.m3 * ak**self.m3 ]
        return slice_arr, hmax
    
    
    def In_Ellipsoid(self, meanfeature, rk, ak ):
#        tree = KDTree(self.xyz_points[:,0:2], leaf_size=2)
        # Select the neighbors around a circular profile
        spatial_meanfeature = np.array( [ meanfeature[0:2].tolist() ] )
        ind = self.tree.query_radius( spatial_meanfeature, r = rk + 0.01 )
        xyz = self.xyz_points[ ind[0] ]
        
        hmax = np.max( xyz[:,2] )          # Compute maximum height
#        h90 = np.percentile( xyz[:,2], 90)          # Compute percentile 90 %
        
        x0 = meanfeature[0]
        y0 = meanfeature[1]
        z0 = meanfeature[2]
        x = xyz[:,0]
        y = xyz[:,1]
        z = xyz[:,2]
        slice_arr = xyz[ ak**2*( (x-x0)**2 + (y-y0)**2 ) + rk**2*(z-z0)**2 < rk**2*ak**2 ]
        return slice_arr, hmax
    
    
    def In_Ellipsoid_Asymmetric(self, meanfeature, rk, ak, bk):
#        tree = KDTree(self.xyz_points[:,0:2], leaf_size=2)
        # Select the neighbors around a circular profile
        spatial_meanfeature = np.array( [ meanfeature[0:2].tolist() ] )
        ind = self.tree.query_radius( spatial_meanfeature, r = rk + 0.01 )
        xyz = self.xyz_points[ ind[0] ]
        
        hmax = np.max( xyz[:,2] )          # Compute maximum height
        
        x0 = meanfeature[0]
        y0 = meanfeature[1]
        z0 = meanfeature[2]
        
        zh = xyz[:,2]
        u_arr =  xyz[ zh >= z0 ]
        x = u_arr[:,0]
        y = u_arr[:,1]
        z = u_arr[:,2]
        u_arr = u_arr[ ak**2*( (x-x0)**2 + (y-y0)**2 ) + rk**2*(z-z0)**2 < rk**2*ak**2 ]
        
        l_arr =  xyz[ zh < z0 ]
        x = l_arr[:,0]
        y = l_arr[:,1]
        z = l_arr[:,2]
        l_arr = l_arr[ bk**2*( (x-x0)**2 + (y-y0)**2 ) + rk**2*(z-z0)**2 < rk**2*bk**2 ]
        
        slice_arr = np.vstack((u_arr,l_arr)) 
        return slice_arr, hmax
    
    
    def Vertical_Distance(self, neighbors, meanfeature):
        h = np.max(neighbors[:,2]) - np.min(neighbors[:,2])
        bottom_distance = np.abs((meanfeature[2] - 0.25*h - neighbors[:,2])/(0.375*h))
        top_distance = np.abs((meanfeature[2] + 0.50*h - neighbors[:,2])/(0.375*h))
        array_distance = np.column_stack((bottom_distance, top_distance))
        min_distance = np.amin(array_distance, axis = 1)
        return min_distance
    
    
    def Vertical_Mask(self, neighbors, meanfeature):
        h = np.max(neighbors[:,2]) - np.min(neighbors[:,2])
        cond1 = neighbors[:,2] >= meanfeature[2] - 0.25*h
        cond2 = neighbors[:,2] <= meanfeature[2] + 0.50*h
        selection_array = np.column_stack((cond1, cond2))
        return np.all(selection_array, axis=1)*1.0
    
    
    def Vertical_Weights(self, neighbors, meanfeature):
        output = self.Vertical_Mask(neighbors, meanfeature)*(1-(1-self.Vertical_Distance(neighbors, meanfeature))**2)
        return output
    
    
    def Horizontal_Weights(self, w, neighbors, meanfeature):    
        distance = LA.norm( neighbors[:,0:2] - meanfeature[0:2], axis = 1 )
        norm_distance = distance/w
        output = np.exp(-5.0*norm_distance**2.0)
        return output
    
    
    def Compute_Mean_feature(self, w, neighbors, meanfeature):
        # Compute the height of the kernel inside the profile
        h = np.max(neighbors[:,2]) - np.min(neighbors[:,2])
        # To deal with cylinders with points over flat plane
        if h > 0.001:    
            horizontal_weights = self.Horizontal_Weights(w, neighbors, meanfeature)
            vertical_weights = self.Vertical_Weights(neighbors, meanfeature)
            # To deal with zero values for vertical weights
            if np.sum(vertical_weights) > 0.001:
                weights = horizontal_weights*vertical_weights
                sum_weights = np.sum(weights)
                new_meanfeature = weights.dot(neighbors) / sum_weights
            else:
                weights = self.Horizontal_Weights(w, neighbors, meanfeature)
                sum_weights = np.sum(weights)
                # To deal with zero values for horizontal weights
                if sum_weights > 0.001:
                    new_meanfeature = weights.dot(neighbors) / sum_weights
                else:
                    new_meanfeature = meanfeature
        else:
            weights = self.Horizontal_Weights(w, neighbors, meanfeature)
            sum_weights = np.sum(weights)
            # To deal with zero values for horizontal weights
            if sum_weights > 0.001:
                new_meanfeature = weights.dot(neighbors) / sum_weights
            else:
                new_meanfeature = meanfeature
                
        return new_meanfeature
        
    
    def Mean_Shift_Ellipsoid( self ):
        mode_list = []       
        i = 0
        for ifeature in self.xyz_points:
            meanfeature = ifeature
            dist_spatial = 100000
            IterCounter = 0 
            rk = self.m1 * self.threshold_height
            
            while ( dist_spatial > self.min_feature_distance ) and (IterCounter < self.max_number_iterations):        
                IterCounter+=1
                oldfeature = meanfeature
                z = meanfeature[2]
                # Equations for controlling the ellipsoid               
                # Linear regressions from height values
                ak = 0.5 * z * self.m2
#                neighbors, hmax = self.In_Ellipsoid( meanfeature, rk, ak )
                neighbors, hmax = self.In_Superellipsoid( meanfeature, rk, ak )
                # Update kernel radius by computing hmax and deriving rt and at
                # rt = ( hmax + self.threshold_height )**self.m2 # Compute maximum radius
                # Crown tree parameters
                at = 0.5 * ( hmax + self.threshold_height )     # Compute tree semi-axis
                alpha = np.log( at * self.m1 ) / np.log( hmax )
                
                rt = hmax**alpha                                # Compute tree radius
                # Kernel radius
                rk = np.sqrt( rt*rt*(2*at*z - z*z)/(at*at) )    # Ellipsoid approach
#                r2 = z*self.m1                                  # Ferraz approach
#                rk = min( r1, r2 )		                        # Radius ponderation
                # To deal with cylinder without points in the neighborhood
                if ( np.shape(neighbors)[0] <= 1 ):
                    meanfeature = oldfeature
                else:
                    meanfeature = self.Compute_Mean_feature(2*rk, neighbors, meanfeature)
                    
                dist_spatial = LA.norm(oldfeature - meanfeature)
            mode_list.append(meanfeature.tolist()+ [ IterCounter ])
            #print("featureID: " + str(i) + " Iter No. : "+ str(IterCounter) + " Dist: " + str(LA.norm(oldfeature[0:2,]-meanfeature[0:2,])) )
            i += 1
        self.xyz_modes = np.asarray(mode_list)
       
    
    def Mean_Shift_Cylinder( self ):
        mode_list = []
        i = 0
        for ifeature in self.xyz_points:
            meanfeature = ifeature
            dist_spatial = 100000
            IterCounter = 0 
            
            while ( dist_spatial > self.min_feature_distance ) and (IterCounter < self.max_number_iterations):        
                IterCounter+=1
                oldfeature = meanfeature
                z = meanfeature[2]
                # Equations for controlling the ellipsoid
                # Select the neighbors around a circular profile
                radius = self.m1 * z
                height = self.m2 * z
                
                neighbors = self.In_Cylinder(meanfeature, radius, height)
                
                # To deal with cylinder without points in the neighborhood
                if ( np.shape(neighbors)[0] <= 1 ):
                    meanfeature = oldfeature
                else:
                    meanfeature = self.Compute_Mean_feature(2*radius, neighbors, meanfeature)
                    
                dist_spatial = LA.norm(oldfeature - meanfeature)
            mode_list.append(meanfeature.tolist() + [ IterCounter ])
            print("featureID: " + str(i) + " Iter No. : "+ str(IterCounter) + " Dist: " + str(LA.norm(oldfeature[0:2,]-meanfeature[0:2,])) )
            i += 1
        self.xyz_modes = np.asarray(mode_list)
        
        
    def Cluster_Points(self):
        # xyz_modes X, Y, Z, NIter 0:3
        clustering = DBSCAN(eps = self.min_cluster_distance, min_samples = self.min_samples).fit(self.xyz_modes[:,0:3])
        self.xyz_labels = clustering.labels_ 
        
    
    def Write_csv(self):
        data_array = np.column_stack((self.xyz_labels, self.xyz_points, self.xyz_modes))
        df_data_array = pd.DataFrame(data_array)
        df_data_array.columns = ['ID','X','Y','Z','Xc','Yc','Zc','NIter']
        df_data_array.to_csv(self.root_csv + self.out_filename)
        print("File "+ self.out_filename + " is saved!")